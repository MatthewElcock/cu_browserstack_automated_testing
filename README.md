# README #

The Browserstack Automated Testing application allows you to test a webpage in various different browsers and devices. 

###Features###

* Configure a test to run on a single, multiple or all major browsers
* Configure a test to run on a single or multiple mobile devices
* Outputs a video displaying the entire page for you to view and find errors in presentation

### How do I get set up? ###

We use npm packages Selenium Webdriver and Prompt to run automated browser tests and easily control user inputs through the command line interface respectively. You must download and install these packages through using the `npm install selenium-webdriver` and `npm install prompt` commands in the command line.

After cloning the source files, execute *CUAutomatedBrowserTesting.js* through the command line by typing `node CUAutomatedBrowserTesting.js` to start the program. Then, follow the instructions on the screen.

### Who do I talk to? ###

Contact ElcockM@cardiff.ac.uk with any questions.

### Known Bugs ###
* Application struggles with mobile testing on iPhone 5
* Opera often encounters various issues, most commonly timeout issues. The test will come back with an error, however the captured video should be enough to view page presentation.