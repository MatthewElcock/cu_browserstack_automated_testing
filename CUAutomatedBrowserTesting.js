var prompt = require('prompt'); //Import Prompt npm package
var tester = new tester_class(); //Create a new tester object

/*User Inteface*/

//User input properties
var schema = {
  properties: {
    project: {
      description: 'Title of Project\n',
      type: 'string',
      required: true,
      message: 'Required'
    },
    build: {
      description: 'Title of Build\n',
      type: 'string',
      required: true,
      message: 'Required'
    },
    name: {
      description: 'Title of Test\n',
      type: 'string',
      required: true,
      message: 'Required'
    },
    device: {
      description: 'Mobile Device\n(Optional, leave blank for desktop testing)\n',
      type: 'string',
      required: false
    },
    device_orientation: {
      description: 'Mobile Device Orientation\n(Optional)\n',
      type: 'string',
      required: false
    },
    url: {
      description: 'Page URL',
      pattern: '^(http|https)://',
      type: 'string',
      required: true,
      message: 'Incorrect input. Make sure your URL begins with http:// or https://'
    },
    browsers: {
      description: 'Test on Browsers\n(To test all browsers, input "All".)\n',
      type: 'string',
      required: false
    },
  }
};

//Welcome message and short introduction
console.log("\nAUTOMATED BROWSER TESTING \n-------------------------\n");
console.log("This application allows you to\ntest a single webpage on different\nbrowsers and devices.\n")
//Test setup -- ask user for test parameters
console.log("TEST SETUP\n")
console.log("*IMPORTANT* If entering multiple values, please use a comma seperated list.\n")

try {
  prompt.get(schema, function(err, result) {

    /*Set object variables*/
    tester.setProject(result.project);
    tester.setBuild(result.build);
    tester.setName(result.name);
    tester.setDevice(result.device);
    tester.setDeviceOrientation(result.device_orientation);
    if (result.device != "" && result.browsers != "") {
      console.log("\nIMPORTANT: One or more browsers were inputted, as well as a device. You may only test either on browsers or mobile devices. Mobile test was kept, and desktop\nremoved. The test will continue as a mobile test with the given device.\n");
      tester.setBrowserName("");
    } else {
      tester.setBrowserName(result.browsers);
    }
    tester.setURL(result.url);

    console.log("\nTest started.")
    console.log("Results available at: https://www.browserstack.com/automate");

    /*Run the test*/
    tester.testPage();

  });
} catch (err) {
  console.log("ERROR: " + err);
}

/*Tester class*/

function tester_class() {

  /*Private variables*/

  var url, project, build, name, device, deviceOrientation, browserName;

  /*Testing methods*/

  this.testPage = function() {

    device.forEach(function(device_element) {
      //For each device
      deviceOrientation.forEach(function(deviceOrientation_element) {
        //For each orientation
        browserName.forEach(function(browserName_element) {

          var webdriver = require('selenium-webdriver'); //Set the webdriver
          // Input capabilities
          var capabilities = {
            'project': project,
            'build': build,
            'name': ((device_element != "") ? name + "-" + device_element + "-" + deviceOrientation_element : name + "-" + browserName_element),
            'device': device_element,
            'deviceOrientation': deviceOrientation_element,
            'browserName': browserName_element,
            'browserstack.user': 'grahambird1',
            'browserstack.key': 'jyAFU4AfQcGdymzKTubo',
          }

          var driver = new webdriver.Builder().
          usingServer('http://hub-cloud.browserstack.com/wd/hub').withCapabilities(capabilities).build();

          driver.get(url);

          if (browserName_element != "opera" && browserName_element != "firefox" && device_element == "") {
            //This command is not supported in opera at the moment (11-10-2017)
            //Browserstack opens a firefox window maximized. Using this command restores down.
            //Command should not be ran on mobile devices
            driver.manage().window().maximize();
          }

          driver.wait(webdriver.until.elementLocated(webdriver.By.css('body'))).then(function() {
            driver.executeScript("$('html, body').animate({scrollTop: $('.footer-global').offset().top}, 20000);")
            setTimeout(function() {
              driver.quit();
            }, 25000);
          });
        });
      });
    });

  }

  /*Getters and setters*/

  this.setURL = function(some_url) {
    url = some_url;
  }

  this.getURL = function() {
    return url;
  }

  this.setProject = function(a_project) {
    project = a_project;
  }

  this.getProject = function() {
    return project;
  }

  this.setBuild = function(a_build) {
    build = a_build;
  }

  this.getBuild = function() {
    return build;
  }

  this.setName = function(a_name) {
    name = a_name;
  }

  this.getName = function() {
    return name;
  }

  this.setDevice = function(some_device) {
    device = some_device.split(",");
  }

  this.getDevice = function() {
    return device.toString();
  }

  this.setDeviceOrientation = function(some_deviceOrientation) {
    deviceOrientation = some_deviceOrientation.split(",");
    deviceOrientation.forEach(function(element, index) {
      deviceOrientation[index] = element.toLowerCase().replace(" ", "");
    })
  }

  this.getDeviceOrientation = function() {
    return deviceOrientation.toString();
  }

  this.setBrowserName = function(some_browser) {
    if (some_browser.toLowerCase() == "all") {
      browserName = ['firefox', 'chrome', 'internet explorer', 'safari', 'edge', 'opera']
    } else {
      browserName = some_browser.split(",");
    }
  }

  this.getBrowserName = function() {
    return browserName.toString();
  }

}
